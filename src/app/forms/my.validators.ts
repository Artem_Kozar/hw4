import {FormControl, ValidationErrors} from '@angular/forms';

export class MyValidators {

  static emailVal(control: FormControl):ValidationErrors {
    if (!(control.value.match(/^[0-9a-zA-Z]+$/))) {
      return {invalidPassword:true};
    }
    return null;
  }



}
