import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MyValidators} from './my.validators';
import {logger} from 'codelyzer/util/logger';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  form: FormGroup;
  constructor() { }
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10)
      ]),
      password: new FormControl('', [Validators.required, this.lettersAndNumberVal]),
      emails: new FormArray([])
    });
  }

  onSubmit(value:any){

  }

  submit() {
    const formData = {...this.form.value};
  }

  addEmail(){
    const control = new FormControl('', Validators.required);
    (this.form.get('emails') as FormArray).push(control);
  }

  lettersAndNumberVal (control: FormControl){
    if ((control.value.match(/^[0-9a-zA-Z]+$/))){
      return null
    }
    return {lettersAndNumberVal:true}
  }


  deleteEmail(idx) {
    console.log(this.form.get('emails').value as FormArray)
    console.log(idx);
    (<FormArray>this.form.get('emails')).removeAt(idx);
  }
}
